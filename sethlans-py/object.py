class Object:
    def __init__(self, properties=None, delegates=None):
        self.properties = properties or {}
        self.delegates = delegates or []

    def get(self, name, visited):
        if self in visited:
            return
        visited.add(self)

        if name in self.properties:
            return self.properties[name]
        else:
            for x in self.delegates:
                y = x.get(name, visited)
                if y is not None:
                    return y

        return None

    def __getitem__(self, name):
        visited = set()
        x = self.get(name, visited)
        if x is None:
            raise KeyError(name)
        return x

    def __setitem__(self, name, value):
        self.properties[name] = value

    def __contains__(self, name):
        visited = set()
        return self.get(name, visited) is not None

    def delegate(self, obj):
        self.delegates = [obj] + self.delegates

    def does_delegate(self, obj, visited=None):
        if self == obj:
            return True

        visited = visited or set()
        if self in visited:
            return False
        visited.add(self)

        for x in self.delegates:
            if x.does_delegate(obj, visited):
                return True

        return False

    def __repr__(self):
        if self.does_delegate(symbol_proto) and self != symbol_proto:
            return '`%r' % self['P_value']['value']
        elif (self.does_delegate(integer_proto) or \
              self.does_delegate(real_proto) or \
              self.does_delegate(list_proto) or \
              self.does_delegate(string_proto)) and \
              'value' in self:
            return repr(self['value'])
        return repr(self.properties)

def make(*args, **kwargs):
    return Object(properties=kwargs, delegates=list(args))

object_proto = make()
list_proto = make(object_proto)
string_proto = make(object_proto)
symbol_proto = make(object_proto)
integer_proto = make(object_proto)
real_proto = make(object_proto)
command_proto = make(object_proto)
query_proto = make(object_proto)
setter_proto = make(command_proto)
getter_proto = make(query_proto)

def make_string(s):
    return make(string_proto, value=s)

symbols = {}
def make_symbol(sym):
    if sym not in symbols:
        symbols[sym] = make(symbol_proto, P_value=make(string_proto, value=sym))
    return symbols[sym]

null_obj = make_symbol('null')
true_obj = make_symbol('true')
false_obj = make_symbol('false')
lt_obj = make_symbol('lt')
eq_obj = make_symbol('eq')
gt_obj = make_symbol('gt')

setter_proto['func'] = lambda self, func, x: \
    self.__setitem__(func['property'], x[0])
setter_proto['P_args'] = make(integer_proto, value=1)

getter_proto['func'] = lambda self, func, x: \
    self[func['property']]
getter_proto['P_args'] = make(integer_proto, value=0)

def property_rw(obj, prop, value=null_obj):
    getter = prop
    setter = prop + ':'
    p = 'P_' + prop

    obj[p] = value
    st = make(setter_proto, property=p)
    obj[make_symbol(setter)] = st
    gt = make(getter_proto, property=p)
    obj[make_symbol(getter)] = gt
    return st, gt

property_rw(object_proto, 'name')
property_rw(symbol_proto, 'value')
property_rw(command_proto, 'args')
property_rw(command_proto, 'code')
property_rw(command_proto, 'scope')
property_rw(query_proto, 'args')
property_rw(query_proto, 'code')
property_rw(query_proto, 'scope')

object_proto['P_name'] = make_symbol('<object>')
list_proto['P_name'] = make_symbol('<list>')
string_proto['P_name'] = make_symbol('<string>')
symbol_proto['P_name'] = make_symbol('<symbol>')
integer_proto['P_name'] = make_symbol('<integer>')
real_proto['P_name'] = make_symbol('<real>')
command_proto['P_name'] = make_symbol('<command>')
query_proto['P_name'] = make_symbol('<query>')
setter_proto['P_name'] = make_symbol('<setter>')
getter_proto['P_name'] = make_symbol('<getter>')

namespace_obj = make(object_proto)
def add_to_ns(x):
    property_rw(namespace_obj, x['P_name']['P_value']['value'], x)
add_to_ns(object_proto)
add_to_ns(list_proto)
add_to_ns(string_proto)
add_to_ns(symbol_proto)
add_to_ns(integer_proto)
add_to_ns(real_proto)
add_to_ns(command_proto)
add_to_ns(query_proto)

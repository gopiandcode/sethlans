from eval import *
from lexer import *
from object import *
from parser import *

def method(mp, proto, name=None):
    def d(f):
        nm = name or f.__code__.co_name
        args = f.__code__.co_argcount - 2
        g = lambda self, func, args: f(self, func, *args)
        proto[make_symbol(nm)] = make(
            mp, func=g, P_args=make(integer_proto, value=args))
        return f
    return d

def query(proto, name=None):
    return method(query_proto, proto, name)
def command(proto, name=None):
    return method(command_proto, proto, name)

def value_op_2(proto, name, f):
    @query(proto, name)
    def g(self, func, other):
        return make(proto, value=f(self['value'], other['value']))
def value_cmp_2(proto, name, f):
    @query(proto, name)
    def g(self, func, other):
        return f(self['value'], other['value'])

value_op_2(integer_proto, '+', lambda x, y: x + y)
value_op_2(integer_proto, '-', lambda x, y: x - y)
value_op_2(integer_proto, '*', lambda x, y: x * y)
value_op_2(integer_proto, '/', lambda x, y: x // y)
value_op_2(integer_proto, '%', lambda x, y: x % y)

def cmp(a, b):
    if a < b:
        return lt_obj
    elif a == b:
        return eq_obj
    elif a > b:
        return gt_obj

value_cmp_2(integer_proto, '<=>', lambda x, y: cmp(x, y))

value_op_2(real_proto, '+', lambda x, y: x + y)
value_op_2(real_proto, '-', lambda x, y: x - y)
value_op_2(real_proto, '*', lambda x, y: x * y)
value_op_2(real_proto, '/', lambda x, y: x / y)

value_cmp_2(real_proto, '<=>', lambda x, y: cmp(x, y))

value_op_2(string_proto, '+', lambda x, y: x + y)
value_cmp_2(string_proto, '=', lambda x, y: x == y)

@query(object_proto, '=')
def equal_object_query(self, func, other):
    return true_obj if self == other else false_obj

@command(object_proto, 'delegate!')
def delegate_object_command(self, func, obj):
    self.delegate(obj)

@query(object_proto, 'delegates?')
def delegates_object_query(self, func, obj):
    return true_obj if self.does_delegate(obj) else false_obj

@query(object_proto, 'not-found?')
def not_found_object_query(self, func, name):
    return null_obj

@query(object_proto, 'not-found!')
def not_found_object_command(self, func, name):
    prop = name['P_value']['value']
    if not prop or prop[-1] != ':':
        return null_obj
    prop = prop[:-1]
    setter, getter = property_rw(self, prop)
    return setter

@command(object_proto, 'def!')
def def_object_command(self, func, name, value):
    self[name] = value

@command(namespace_obj, 'print!')
def print_ns_command(self, func, x):
    print(x)

@query(namespace_obj, 'read?')
def read_ns_query(self, func):
    return make(string_proto, value=input())

@query(namespace_obj, 'command?')
def command_ns_query(self, func, args, code):
    return make(
        command_proto, args=args['value'],
        P_args=make(integer_proto, value=len(args['value'])),
        P_code=code, P_scope=self)

@query(namespace_obj, 'query?')
def query_ns_query(self, func, args, code):
    return make(
        query_proto, args=args['value'],
        P_args=make(integer_proto, value=len(args['value'])),
        P_code=code, P_scope=self)

def make_scope(self, func, args):
    scope = make(self, func['P_scope'])
    property_rw(scope, 'self', self)
    property_rw(scope, 'scope', func['P_scope'])
    for k, v in zip(func['args'], args):
        property_rw(scope, k['P_value']['value'], v)
    return scope

def do_command(self, func, args):
    scope = make_scope(self, func, args)
    eval(scope, func['P_code']['value'])
command_proto['func'] = do_command

def do_query(self, func, args):
    scope = make_scope(self, func, args)
    eval(scope, func['P_code']['value'], True)
    return scope['P_return']
query_proto['func'] = do_query

@query(query_proto, 'apply?')
@command(command_proto, 'apply!')
def apply_qc(self, func, args):
    return call_func(self, self, args['value'])

@command(namespace_obj, 'eval!')
def eval_command(self, func, code):
    if not code.does_delegate(list_proto):
        raise RuntimeError('can only eval list')
    eval(self, code['value'])

@query(string_proto, 'parse?')
def parse_string_query(self, func):
    from lexer import Lexer
    from parser import Parser
    return Parser(Lexer(iter(self['value'])).lex()).parse()

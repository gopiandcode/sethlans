import object

def call_func(obj, self, args):
    return obj['func'](self, obj, args)

S_nf_q = object.make_symbol('not-found?')
S_nf_c = object.make_symbol('not-found!')

def try_get_q(self, name):
    return (name in self and self[name].does_delegate(object.query_proto)) or \
        call_func(self[S_nf_q], self, [name]) != object.null_obj
def try_get_c(self, name):
    return (name in self and \
            self[name].does_delegate(object.command_proto)) or \
        call_func(self[S_nf_c], self, [name]) != object.null_obj

def get_or_q(self, name):
    if name in self:
        return self[name]
    return call_func(self[S_nf_q], self, [name])
def get_or_c(self, name):
    if name in self:
        return self[name]
    return call_func(self[S_nf_c], self, [name])

def q_empty():
    return eval_queue[0] is None

def eval_terminal(mself, self):
    if eval_queue[0].does_delegate(object.symbol_proto):
        if eval_queue[0]['P_value']['value'] == '`':
            eval_queue.pop(0)
            return eval_queue.pop(0)
        elif eval_queue[0]['P_value']['value'] == '(':
            eval_queue.pop(0)
            x = eval_expr(mself, mself)
            if eval_queue.pop(0)['P_value']['value'] != ')':
                raise SyntaxError('expected )')
            return x
        else:
            if not try_get_q(mself, eval_queue[0]):
                return mself

            f = get_or_q(mself, eval_queue.pop(0))
            return call_func(f, mself, [])
    else:
        return eval_queue.pop(0)

def eval_expr(mself, self):
    if eval_queue[0].does_delegate(object.symbol_proto) and \
       try_get_q(self, eval_queue[0]):
        f = get_or_q(self, eval_queue.pop(0))

        args = []
        for _ in range(f['P_args']['value']):
            args.append(eval_terminal(mself, self))

        x = call_func(f, self, args)
        if eval_queue and try_get_q(x, eval_queue[0]):
            return eval_expr(mself, x)
        return x
    else:
        x = eval_terminal(mself, self)
        if eval_queue and try_get_q(x, eval_queue[0]):
            return eval_expr(mself, x)
        return x

def _eval():
    self = None
    stack = []
    while eval_queue:
        if eval_queue[0] == 1:
            eval_queue.pop(0)
            stack.append(self)
            self = eval_queue.pop(0)

        while eval_queue and eval_queue[0] is None:
            eval_queue.pop(0)
            self = stack.pop()
            if not stack:
                return

        x = eval_expr(self, self)
        if q_empty():
            raise SyntaxError('unexpected end of code')
        if not try_get_c(x, eval_queue[0]):
            raise SyntaxError(
                'invalid command %s' % eval_queue[0]['P_value']['value'])
        f = get_or_c(x, eval_queue.pop(0))
        args = []

        for _ in range(f['P_args']['value']):
            args.append(eval_terminal(self, self))

        call_func(f, x, args)

eval_queue = []
in_eval = False
def eval(self, objs, force=False):
    global eval_queue, in_eval
    eval_queue = [1, self] + objs + [None] + eval_queue
    if not in_eval or force:
        in_eval = True
        _eval()
        if not force:
            in_eval = False

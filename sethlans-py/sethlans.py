#!/usr/bin/env python3

import definitions
from eval import eval
from lexer import Lexer
from object import namespace_obj
from parser import Parser
import sys

def eval_it(it):
    eval(namespace_obj, Parser(Lexer(it).lex()).parse_all()['value'])

if __name__ == '__main__':
    for x in sys.argv[1:]:
        with open(x) as f:
            eval_it(iter(f.read()))

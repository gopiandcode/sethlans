class Lexer:
    WS = ' \t\v\r\n\f'
    HEX = '0123456789ABCDEFabcdef'

    def __init__(self, it):
        self.it = it
        self.c = ' '
        self.eof = False

    def next(self):
        try:
            self.c = next(self.it)
        except StopIteration:
            self.eof = True
            raise

        return self.c

    def comment(self):
        n = 0
        while not self.eof:
            try:
                self.next()
            except StopIteration:
                raise SyntaxError('unexpected EOF')

            if self.c == '}':
                if n == 0:
                    break
                n -= 1
            elif self.c == '{':
                n += 1

    def skip_ws(self):
        while not self.eof:
            if self.c == '{':
                self.comment()
            elif self.c not in self.WS:
                break

            self.next()

    def char(self):
        if self.c == '$':
            try:
                c = self.next()
            except StopIteration:
                raise SyntaxError('unexpected EOF')

            return c
        elif self.c == '@':
            try:
                h = self.next()
                l = self.next()
            except StopIteration:
                raise SyntaxError('unexpected EOF')

            if h not in self.HEX or l not in self.HEX:
                raise SyntaxError('invalid escape')

            return chr(int(h + l, 16))
        return self.c

    def string(self):
        s = ''
        while not self.eof:
            try:
                self.next()
            except StopIteration:
                raise SyntaxError('unexpected EOF')

            if self.c == "'":
                return s
            elif self.c == ' ':
                s += ' '
            elif self.c in self.WS:
                raise SyntaxError('invalid whitespace in quotes')
            else:
                s += self.char()

    def tok_part(self):
        if self.c in self.WS:
            raise StopIteration()

        if self.c == "'":
            return self.string()
        elif self.c == '{':
            self.comment()
            return ''
        elif self.c in self.WS:
            raise StopIteration()
        else:
            return self.char()

    def token(self):
        tok = ""

        while not self.eof:
            try:
                tok += self.tok_part()
                self.next()
            except StopIteration:
                break

        return tok

    def lex(self):
        while not self.eof:
            try:
                self.skip_ws()
            except StopIteration:
                return

            yield self.token()

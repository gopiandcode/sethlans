import object
import re

class Parser:
    ALPHABET = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'

    def __init__(self, it):
        self.it = it
        self.eof = False

        try:
            self.next()
        except StopIteration:
            raise SyntaxError('empty')

    def next(self):
        try:
            self.tok = next(self.it)
        except StopIteration:
            self.eof = True
            raise

        return self.tok

    def try_num(self, base):
        x = self.tok
        m = 1
        if x[0] in '+-':
            if x[0] == '-':
                m = -1
            x = x[1:]

        if not x:
            return

        start = '%d#' % base
        if x.startswith(start):
            x = x[len(start):]
        elif base != 10:
            return

        alpha = self.ALPHABET[:base]
        i = 0
        while x:
            if x[0].upper() in alpha:
                i = (i * base) + alpha.index(x[0].upper())
                x = x[1:]
            else:
                break

        if not x:
            return m * i
        if x[0] != '.':
            return
        x = x[1:]

        j = 0
        jj = 1
        while x:
            if x[0].upper() in alpha:
                j = (j * base) + alpha.index(x[0].upper())
                jj *= base
                x = x[1:]
            else:
                break

        n = i + j / jj
        if not x:
            return m * n
        if x[0] != '^':
            return
        x = x[1:]

        if not x:
            return

        alpha = self.ALPHABET[:10]
        mm = 1
        if x[0] in '+-':
            if x[0] == '-':
                mm = -1
            x = x[1:]

        if not x:
            return

        nn = 0
        while x:
            if x[0].upper() in alpha:
                nn = (nn * 10) + alpha.index(x[0].upper())
                x = x[1:]
            else:
                break

        if not x:
            return m * n * base ** (mm * nn)

        return

    def parse(self):
        if not self.tok:
            return object.make_symbol('')
        elif self.tok[0] == '"':
            return object.make_string(self.tok[1:])
        elif self.tok[0] == '`':
            return object.make_symbol(self.tok[1:])
        elif self.tok == '[':
            try:
                self.next()

                l = []
                while self.tok != ']':
                    l.append(self.parse())
                    self.next()
            except StopIteration:
                raise SyntaxError('unexpected EOF')

            return object.make(object.list_proto, value=l)
        elif self.tok == '[[':
            try:
                self.next()

                d = {}
                while self.tok != ']]':
                    x = self.tok
                    self.next()

                    y = self.parse()
                    self.next()

                    if x in d:
                        raise SyntaxError('duplicate property')
                    d[x] = y
            except StopIteration:
                raise SyntaxError('unexpected EOF')

            obj = object.make(object.object_proto)
            for k, v in d.items():
                object.property_rw(obj, k, v)
            return obj
        else:
            for i in range(2, 37):
                x = self.try_num(i)
                if isinstance(x, int):
                    return object.make(object.integer_proto, value=x)
                elif isinstance(x, float):
                    return object.make(object.real_proto, value=x)

            return object.make_symbol(self.tok)

    def parse_all(self):
        x = self.parse()

        try:
            self.next()
        except StopIteration:
            pass
        else:
            raise SyntaxError('only one poken allowed')

        return x

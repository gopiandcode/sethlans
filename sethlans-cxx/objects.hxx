#pragma once

#include "object.hxx"

#include <cstddef>
#include <gmpxx.h>
#include <string>

class Objects_ {
public:
    // prototypes
    Object* p_object;
    Object* p_unique;
    Object* p_symbol;
    Object* p_buf8;
    Object* p_string;
    Object* p_list;
    Object* p_integer;
    Object* p_real;
    Object* p_method;
    Object* p_native_method;
    Object* p_eval_method;
    Object* p_accessor;
    Object* p_command;
    Object* p_query;
    Object* p_native_command;
    Object* p_native_query;
    Object* p_setter;
    Object* p_getter;
    Object* p_eval_command;
    Object* p_eval_query;
    Object* p_constant;
    Object* p_scope;

    // useful symbols
    Object* s__argcount;
    Object* s__argnames;
    Object* s__car;
    Object* s__cdr;
    Object* s__code;
    Object* s__context;
    Object* s__name;
    Object* s__property;
    Object* s__scope;
    Object* s__self;
    Object* s__value;

    Object* s_not_found_c;
    Object* s_not_found_q;
    Object* s_clone_q;

    Object* s_true;
    Object* s_false;
    Object* s_lt;
    Object* s_eq;
    Object* s_gt;

    Object* sx_backtick;
    Object* sx_backslash;
    Object* sx_lparen;
    Object* sx_rparen;

    // non-symbol singletons
    Object* g_nil;

    // interned ints (-128..127)
    Object* integers[256];

    void init();
};

extern Objects_ objects;

// make setter/getter
void make_sg(
    Object* obj, std::u8string prop,
    Object* value = nullptr, bool force = false);

// make constant
void make_const(Object* obj, std::u8string prop, Object* value);

// get the string of a symbol
std::u8string get_sym_string(Object* obj);

// make a string object
Object* make_string(std::u8string str);

// make an integer object
Object* make_integer(mpz_class n);

// make a real object
Object* make_real(double n);

// get a symbol from its name
// two symbols with the same name are the same object
Object* get_symbol(std::u8string symbol);

// make a linked list cons
Object* cons(Object* car, Object* cdr);

// linked list builder
class List_builder {
    Object* first;
    Object* last;
public:
    List_builder() noexcept;
    void append(Object* obj);
    Object* finish() const noexcept;
};

// linked list iterator
class List_iterator {
    Object* obj;

public:
    List_iterator() noexcept;
    constexpr List_iterator(Object* x) noexcept
        : obj(x) { }

    Object* operator*() const;
    List_iterator& operator++();
    List_iterator operator++(int);

    constexpr bool operator==(const List_iterator& other) const noexcept {
        return obj == other.obj;
    }
};

// get the length of a list
std::size_t list_length(Object* list);

// print an object
void dump_object(Object* obj, unsigned indent = 0);

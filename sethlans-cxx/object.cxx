#include "object.hxx"

#include <algorithm>
#include <cstddef>
#include <cstdint>
#include <list>
#include <optional>
#include <stdexcept>

using namespace std;

size_t Object::Hasher::operator()(Object* const& x) const noexcept {
    static_assert(
        sizeof(size_t) == sizeof(uint64_t),
        "Only 64-bit is supported");

    auto n = reinterpret_cast<uint64_t>(x);
    // SplitMix64
    n ^= n >> 30;
    n *= UINT64_C(0xBF58476D1CE4E5B9);
    n ^= n >> 27;
    n *= UINT64_C(0x94D049BB133111EB);
    n ^= n >> 31;
    return n;
}

Object::Object() {
    update_res_order();
}

Object* Object::clone() const {
    auto obj = new Object();
    obj->delegated = delegated;
    obj->update_res_order();

    for (auto prop : properties)
        obj->properties[prop.first] = prop.second;

    obj->native_data = native_data;

    return obj;
}

optional<Object*> Object::get(Object* name) const noexcept {
    for (auto obj : res_order) {
        auto it = obj->properties.find(name);
        if (it != obj->properties.end())
            return it->second;
    }
    return nullopt;
}

void Object::set(Object* name, Object* value) {
    properties[name] = value;
}

Object::Native_data& Object::get_nd() noexcept {
    for (auto obj : res_order) {
        if (obj->native_data)
            return obj->native_data;
    }
    return native_data;
}

void Object::set_nd(Object::Native_data data) noexcept {
    native_data = data;
}

void Object::delegate(Object* obj) {
    if (!obj->delegates.contains(this)) {
        delegated.push_front(obj);
        if (!obj->does_delegate(this))
            obj->delegates.insert(this);
        update_res_order();
    }
}

void Object::undelegate(Object* obj) {
    auto it = find(delegated.begin(), delegated.end(), obj);
    if (it != delegated.end()) {
        auto it1 = obj->delegates.find(this);
        if (it1 != obj->delegates.end())
            obj->delegates.erase(it1);
        delegated.erase(it);
        update_res_order();
    }
}

bool Object::does_delegate(Object* obj) const noexcept {
    return find(res_order.begin(), res_order.end(), obj)
        != res_order.end();
}

void Object::update_res_order() {
    res_order.clear();
    res_order.push_front(this);

    list<list<Object*>> to_merge;
    for (auto auth : delegated)
        to_merge.push_back(auth->res_order);

    for (;;) {
        bool found = false;
        bool not_empty = false;
        for (auto& pro : to_merge) {
            if (pro.empty())
                continue;
            not_empty = true;

            auto head = pro.front();
            bool valid = true;
            for (auto& pro2 : to_merge) {
                if (pro2.empty())
                    continue;

                auto b = pro2.begin();
                b++;
                if (find(b, pro2.end(), head) != pro2.end()) {
                    valid = false;
                    break;
                }
            }

            if (!valid)
                continue;

            res_order.push_back(head);

            for (auto& pro2 : to_merge) {
                if (pro2.empty())
                    continue;

                if (pro2.front() == head)
                    pro2.pop_front();
            }

            found = true;
            break;
        }

        if (!not_empty)
            break;
        if (!found)
            throw runtime_error("delegation hierarchy is not linearizable");
    }

    for (auto delegate : delegates)
        delegate->update_res_order();
}

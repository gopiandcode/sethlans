#include "object.hxx"
#include "objects.hxx"
#include "parser.hxx"

#include <cassert>
#include <cmath>
#include <gmpxx.h>
#include <optional>
#include <stdexcept>
#include <variant>
#include <vector>

using namespace std;

static optional<unsigned> get_digit(char8_t ch, unsigned base) {
    unsigned d = 0;
    if (ch >= '0' && ch <= '9')
        d = ch - '0';
    else if (ch >= 'A' && ch <= 'Z')
        d = ch - 'A' + 10;
    else if (ch >= 'a' && ch <= 'z')
        d = ch - 'a' + 10;
    else
        return nullopt;

    if (d < base)
        return d;
    return nullopt;
}

static optional<Object*> try_parse_num(vector<char8_t>& s) {
    auto it = s.begin();
    if (it == s.end())
        return nullopt;

    bool negate = false;
    if (*it == '+') {
        it++;
    } else if (*it == '-') {
        negate = true;
        it++;
    }

    if (it == s.end())
        return nullopt;

    unsigned base = 10;
    mpz_class n_int(0);

    bool ok_int = false;
    while (it != s.end() && get_digit(*it, base)) {
        ok_int = true;
        const auto d = get_digit(*it, base).value();

        n_int = n_int * base + d;

        if (++it == s.end())
            break;
        if (*it == '#') {
            ok_int = false;
            it++;

            if (n_int < 2 || n_int > 36)
                return nullopt;

            base = n_int.get_si();
            n_int = 0;
        }
    }

    if (it == s.end()) {
        if (!ok_int)
            return nullopt;
        else
            return make_integer(negate ? -n_int : n_int);
    }

    if (*it != '.')
        return nullopt;
    it++;

    mpz_class n_frac_div(1);
    mpz_class n_frac(0);

    while (it != s.end() && get_digit(*it, base)) {
        const auto d = get_digit(*it, base).value();
        n_frac_div *= base;
        n_frac = n_frac * base + d;
        it++;
    }

    bool exp_negate = false;
    mpz_class n_exp(0);

    if (it != s.end()) {
        if (*it == '^') {
            it++;

            if (it == s.end())
                return nullopt;

            if (*it == '+') {
                it++;
            } else if (*it == '-') {
                it++;
                exp_negate = true;
            }

            while (it != s.end() && get_digit(*it, 10)) {
                const auto d = get_digit(*it, 10).value();
                n_exp = n_exp * 10 + d;
                it++;
            }

            if (it != s.end())
                return nullopt;
        } else {
            return nullopt;
        }
    }

    mpq_class t(n_frac, n_frac_div);
    t += mpq_class(n_int);

    auto d = t.get_d();
    if (negate)
        d = -d;

    long exp = 0;
    if (!n_exp.fits_slong_p())
        exp = 100000;
    else
        exp = n_exp.get_si();
    if (exp_negate)
        exp = -exp;

    d *= pow(base, exp);

    return make_real(d);
}

#include <iostream>
class Parser {
public:
    List_iterator it;

    Parser(Object* t)
        : it(t) { }

    Object* parse() {
        if (*it == get_symbol(u8"[")) {
            List_builder list;

            it++;
            while (*it != get_symbol(u8"]"))
                list.append(parse());
            it++;

            return list.finish();
        } else if (*it == get_symbol(u8"[[")) {
            auto obj = new Object();
            obj->delegate(objects.p_object);

            it++;
            while (*it != get_symbol(u8"]]")) {
                auto name = *it++;
                auto value = parse();

                if (obj->get(name))
                    throw runtime_error("duplicate property");
                make_sg(obj, get_sym_string(name), value);
            }
            it++;

            return obj;
        } else {
            auto x = *it++;

            auto& nd = get<T_buf8>(
                x->get(objects.s__name).value()->
                get_nd().value());

            if (nd[0] == '"') {
                auto str = new Object();
                str->delegate(objects.p_string);

                vector<char8_t> buf(nd.begin() + 1, nd.end());
                str->set_nd(buf);

                return str;
            } else if (nd[0] == '`') {
                u8string ns(nd.begin() + 1, nd.end());
                return get_symbol(ns);
            }

            auto y = try_parse_num(nd);
            if (y)
                return y.value();

            return x;
        }
    }
};

Object* parse(Object* tokens) {
    if (tokens == objects.g_nil)
        throw runtime_error("empty");

    Parser p(tokens);
    auto x = p.parse();

    if (p.it != List_iterator())
        throw runtime_error("trailing tokens");

    return x;
}

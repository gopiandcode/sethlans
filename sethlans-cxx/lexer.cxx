#include "lexer.hxx"
#include "object.hxx"
#include "objects.hxx"

#include <optional>
#include <stdexcept>
#include <string>

using namespace std;

static bool is_space(char8_t c) {
    return c <= ' ';
}

static optional<unsigned> hex(char8_t c) {
    if (c >= '0' && c <= '9')
        return c - '0';
    else if (c >= 'A' && c <= 'F')
        return c - 'A' + 10;
    else if (c >= 'a' && c <= 'f')
        return c - 'a' + 10;
    return nullopt;
}

static void put_utf8(u8string& s, char32_t n) {
    if (n < 0x80) {
        s.push_back(static_cast<char8_t>(n));
    } else if (n < 0x800) {
        s.push_back(0b11000000 | static_cast<char8_t>(n >> 6));
        s.push_back(0b10000000 | static_cast<char8_t>(n & 63));
    } else if (n < 0x10000) {
        s.push_back(0b11100000 | static_cast<char8_t>(n >> 12));
        s.push_back(0b10000000 | static_cast<char8_t>((n >> 6) & 63));
        s.push_back(0b10000000 | static_cast<char8_t>(n & 63));
    } else {
        s.push_back(0b11110000 | static_cast<char8_t>(n >> 18));
        s.push_back(0b10000000 | static_cast<char8_t>((n >> 12) & 63));
        s.push_back(0b10000000 | static_cast<char8_t>((n >> 6) & 63));
        s.push_back(0b10000000 | static_cast<char8_t>(n & 63));
    }
}

struct Lexer {
    u8string::iterator cur, end;

    Lexer(u8string& code) {
        cur = code.begin();
        end = code.end();
    }

    void skip_comment() {
        unsigned n = 0;
        for (;;) {
            if (cur == end) {
                throw runtime_error("unexpected_eof");
            } else if (*cur == '{') {
                cur++;
                n++;
            } else if (*cur == '}') {
                cur++;
                if (n-- == 0)
                    break;
            } else {
                cur++;
            }
        }
    }

    void skip_space() {
        while (cur != end) {
            if (is_space(*cur)) {
                cur++;
            } else if (*cur == '{') {
                cur++;
                skip_comment();
            } else {
                break;
            }
        }
    }

    void esc_char(u8string& s) {
        if (*cur == '$') {
            if (++cur == end)
                throw runtime_error("unexpected eof");
            s.push_back(*cur++);
        } else if (*cur == '@') {
            cur++;
            char32_t n = 0;
            for (unsigned i = 0;; i++) {
                if (cur == end) {
                    throw runtime_error("unexpected eof");
                } else if (*cur == '@') {
                    if (n > 0x10FFFF)
                        throw runtime_error("codepoint too large");
                    cur++;
                    put_utf8(s, n);
                    break;
                } else if (*cur == '#') {
                    if (n > 0x100)
                        throw runtime_error("codepoint too large");
                    cur++;
                    s.push_back(static_cast<char8_t>(n));
                    break;
                }
                if (i >= 6)
                    throw runtime_error("codepoint too large");
                const auto h = hex(*cur++);
                if (!h)
                    throw runtime_error("invalid hex digit");
                n = (n << 4) | h.value();
            }
        } else {
            s.push_back(*cur++);
        }
    }

    void quoted(u8string& s) {
        for (;;) {
            if (cur == end) {
                throw runtime_error("unexpected eof");
            } else if (*cur == '\'') {
                cur++;
                break;
            } else {
                esc_char(s);
            }
        }
    }

    optional<u8string> token() {
        skip_space();
        if (cur == end)
            return nullopt;

        u8string s;

        while (cur != end) {
            if (is_space(*cur)) {
                break;
            } else if (*cur == '{') {
                cur++;
                skip_comment();
            } else if (*cur == '\'') {
                cur++;
                quoted(s);
            } else {
                esc_char(s);
            }
        }

        return s;
    }
};

Object* lex(u8string& code) {
    Lexer lexer(code);
    List_builder tokens;

    for (;;) {
        auto tok = lexer.token();
        if (!tok)
            break;

        tokens.append(get_symbol(tok.value()));
    }

    return tokens.finish();
}

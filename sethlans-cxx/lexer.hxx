#pragma once

#include "object.hxx"

#include <string>

// tokenize code into a <list> of <symbol>s
Object* lex(std::u8string& code);

#include "bootenv.hxx"
#include "eval.hxx"
#include "lexer.hxx"
#include "objects.hxx"
#include "parser.hxx"

#include <gc.h>
#include <iostream>
#include <string>

using namespace std;

int main() {
    GC_INIT();

    objects.init();
    auto env = make_bootenv();

    u8string str;
    for (;;) {
        auto c = cin.get();
        if (c == istream::traits_type::eof())
            break;
        str.push_back(c);
    }

    auto tokens = lex(str);
    auto poken = parse(tokens);

    Eval(env, poken).commands();

    return 0;
}

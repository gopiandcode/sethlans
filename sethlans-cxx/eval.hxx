#pragma once

#include "object.hxx"
#include "objects.hxx"

class Eval {
private:
    // top-level self object
    Object* t_self;
    // code
    List_iterator code;

    Object* terminal();
    Object* expr(Object* self, bool first = true);

    void command(Object*& self_r);
public:
    constexpr Eval(Object* self, Object* code)
        : t_self(self), code(code) { }

    // eval a list of commands
    void commands();
    // eval_commands but allows a trailing expression as return value
    Object* query();
};

#include "eval.hxx"
#include "object.hxx"
#include "objects.hxx"

#include <cstddef>
#include <gmpxx.h>
#include <iostream>
#include <string>
#include <type_traits>
#include <unordered_map>
#include <variant>
#include <vector>

using namespace std;

static unordered_map<u8string, Object*> symbols;
static Object* s__name;

Objects_ objects;

static void init_sym();

static Object* make_scope(
    Object* self, Object* scope, Object* context,
    Object* argnames, list<Object*>& args) {
    auto obj = new Object();

    obj->delegate(scope);
    obj->delegate(objects.p_scope);
    obj->delegate(self);

    obj->set(objects.s__self, self);
    obj->set(objects.s__scope, scope);
    obj->set(objects.s__context, context);

    List_iterator it(argnames);
    for (auto x : args)
        make_sg(obj, get_sym_string(*it++), x, true);

    return obj;
}

template<typename T>
static Object* make_with_nd(Object* parent, T nd) {
    auto obj = new Object();
    obj->delegate(parent);
    obj->set_nd(nd);
    return obj;
}

void Objects_::init() {
    p_object = new Object();

    p_unique = new Object();
    p_unique->delegate(p_object);

    p_symbol = new Object();
    p_symbol->delegate(p_unique);

    p_buf8 = new Object();
    p_buf8->delegate(p_object);

    p_string = new Object();
    p_string->delegate(p_buf8);

    init_sym();

    s__argcount = get_symbol(u8"_argcount");
    s__argnames = get_symbol(u8"_argnames");
    s__car = get_symbol(u8"_car");
    s__cdr = get_symbol(u8"_cdr");
    s__code = get_symbol(u8"_code");
    s__context = get_symbol(u8"_context");
    s__name = get_symbol(u8"_name");
    s__property = get_symbol(u8"_property");
    s__scope = get_symbol(u8"_scope");
    s__self = get_symbol(u8"_self");
    s__value = get_symbol(u8"_value");

    s_not_found_c = get_symbol(u8"not-found!");
    s_not_found_q = get_symbol(u8"not-found?");
    s_clone_q = get_symbol(u8"clone?");

    s_true = get_symbol(u8"true");
    s_false = get_symbol(u8"false");
    s_lt = get_symbol(u8"lt");
    s_eq = get_symbol(u8"eq");
    s_gt = get_symbol(u8"gt");

    sx_backtick = get_symbol(u8"`");
    sx_backslash = get_symbol(u8"\\");
    sx_lparen = get_symbol(u8"(");
    sx_rparen = get_symbol(u8")");

    p_object->set(s__name, get_symbol(u8"<object>"));
    p_unique->set(s__name, get_symbol(u8"<unique>"));
    p_symbol->set(s__name, get_symbol(u8"<symbol>"));
    p_buf8->set(s__name, get_symbol(u8"<buf8>"));
    p_string->set(s__name, get_symbol(u8"<string>"));

    auto create_obj = [&](auto& ptr, auto parent, auto name) {
        ptr = new Object();
        ptr->delegate(parent);
        ptr->set(s__name, get_symbol(name));
        return ptr;
    };

    create_obj(p_list, p_object, u8"<list>");
    create_obj(p_integer, p_unique, u8"<integer>");
    create_obj(p_real, p_unique, u8"<real>");
    create_obj(p_method, p_object, u8"<method>");
    create_obj(p_native_method, p_method, u8"<native-method>");
    create_obj(p_eval_method, p_method, u8"<eval-method>");
    create_obj(p_accessor, p_native_method, u8"<accessor>");
    create_obj(p_command, p_method, u8"<command>");
    create_obj(p_query, p_method, u8"<query>");
    create_obj(p_native_command, p_command, u8"<eval-command>")
        ->delegate(p_native_method);
    create_obj(p_native_query, p_query, u8"<eval-query>")
        ->delegate(p_native_method);
    create_obj(p_setter, p_native_command, u8"<setter>")
        ->delegate(p_accessor);
    create_obj(p_getter, p_native_query, u8"<getter>")
        ->delegate(p_accessor);
    create_obj(p_eval_command, p_command, u8"<eval-command>")
        ->delegate(p_eval_method);
    create_obj(p_eval_query, p_query, u8"<eval-query>")
        ->delegate(p_eval_method);
    create_obj(p_constant, p_native_query, u8"<constant>");
    create_obj(p_scope, p_object, u8"<scope>");

    create_obj(g_nil, p_unique, u8"<nil>");

    for (signed x = -128; x < 128; x++)
        integers[x + 128] = make_with_nd(p_integer, mpz_class(x));

    p_setter->set(s__argcount, make_integer(1));
    p_setter->set_nd([](auto fn, auto, auto self, auto args) {
        self->set(fn->get(objects.s__property).value(), args.front());
    });

    p_getter->set(s__argcount, make_integer(0));
    p_getter->set_nd(T_query([](auto fn, auto, auto self, auto) {
        return self->get(fn->get(objects.s__property).value()).value();
    }));

    p_eval_command->set_nd([](auto fn, auto ctx, auto self, auto args) {
        auto argnames = fn->get(objects.s__argnames).value();
        auto code = fn->get(objects.s__code).value();
        auto scope = fn->get(objects.s__scope).value();

        auto sc = make_scope(self, scope, ctx, argnames, args);
        Eval(sc, code).commands();
    });

    p_eval_query->set_nd(T_query([](auto fn, auto ctx, auto self, auto args) {
        auto argnames = fn->get(objects.s__argnames).value();
        auto code = fn->get(objects.s__code).value();
        auto scope = fn->get(objects.s__scope).value();

        auto sc = make_scope(self, scope, ctx, argnames, args);
        return Eval(sc, code).query();
    }));

    p_constant->set(s__argcount, make_integer(0));
    p_constant->set_nd(T_query([](auto fn, auto, auto, auto) {
        return fn->get(objects.s__value).value();
    }));
}

void make_sg(Object* obj, u8string prop, Object* value, bool force) {
    value = value ? value : objects.g_nil;
    auto p = get_symbol(u8"_" + prop);
    auto s = get_symbol(prop + u8":");
    auto g = get_symbol(prop);

    if (!obj->get(p) || force)
        obj->set(p, value);

    auto st = new Object();
    st->delegate(objects.p_setter);
    st->set(objects.s__property, p);
    obj->set(s, st);

    auto gt = new Object();
    gt->delegate(objects.p_getter);
    gt->set(objects.s__property, p);
    obj->set(g, gt);
}

void make_const(Object* obj, u8string prop, Object* value) {
    auto c = new Object();
    c->delegate(objects.p_constant);
    c->set(objects.s__value, value);
    obj->set(get_symbol(prop), c);
}

u8string get_sym_string(Object* obj) {
    auto buf = get<T_buf8>(
        obj->get(objects.s__name).value()->
        get_nd().value());
    return u8string(buf.begin(), buf.end());
}

Object* make_string(u8string data) {
    vector<char8_t> buf(data.begin(), data.end());
    return make_with_nd(objects.p_string, buf);
}

Object* make_integer(mpz_class n) {
    if (n >= -128 && n <= 127)
        return objects.integers[n.get_si() + 128];
    return make_with_nd(objects.p_integer, n);
}

Object* make_real(double n) {
    return make_with_nd(objects.p_real, n);
}

static void init_sym() {
    s__name = new Object();
    s__name->delegate(objects.p_symbol);
    s__name->set(s__name, make_string(u8"_name"));
    symbols[u8"_name"] = s__name;
}

Object* get_symbol(u8string symbol) {
    auto it = symbols.find(symbol);
    if (it == symbols.end()) {
        auto obj = new Object();
        obj->delegate(objects.p_symbol);
        obj->set(s__name, make_string(symbol));

        symbols[symbol] = obj;
        return obj;
    }
    return it->second;
}

Object* cons(Object* car, Object* cdr) {
    auto obj = new Object();
    obj->delegate(objects.p_list);
    obj->set(objects.s__car, car);
    obj->set(objects.s__cdr, cdr);
    return obj;
}

List_builder::List_builder() noexcept {
    first = objects.g_nil;
    last = objects.g_nil;
}

void List_builder::append(Object* obj) {
    auto n = cons(obj, objects.g_nil);
    if (last == objects.g_nil) {
        first = last = n;
    } else {
        last->set(objects.s__cdr, n);
        last = n;
    }
}

Object* List_builder::finish() const noexcept {
    return first;
}

List_iterator::List_iterator() noexcept
    : obj(objects.g_nil) { }

Object* List_iterator::operator*() const {
    return obj->get(objects.s__car).value();
}

List_iterator& List_iterator::operator++() {
    obj = obj->get(objects.s__cdr).value();
    return *this;
}

List_iterator List_iterator::operator++(int) {
    List_iterator t(*this);
    ++*this;
    return t;
}

size_t list_length(Object* list) {
    size_t n = 0;
    List_iterator it(list);
    while (it != List_iterator()) {
        n++;
        it++;
    }
    return n;
}

static void do_indent(unsigned n) {
    while (n--) cout << "  ";
}

static void cout_u8(u8string s) {
    for (auto c : s)
        cout << static_cast<char>(c);
}

void dump_object(Object* obj, unsigned indent) {
    cout << "[[" << endl;

    auto& nd = obj->get_nd();
    if (nd) {
        auto& ndv = nd.value();
        do_indent(indent + 1);
        cout << "(native) ";

        visit([](auto&& arg) {
            using T = decay_t<decltype(arg)>;
            if constexpr (is_same_v<T, T_buf8>) {
                cout << "'\"";
                cout_u8(u8string(arg.begin(), arg.end()));
                cout << '\'' << endl;
            } else if constexpr (is_same_v<T, T_command>) {
                cout << "<command>" << endl;
            } else if constexpr (is_same_v<T, T_query>) {
                cout << "<query>" << endl;
            } else {
                cout << arg << endl;
            }
        }, ndv);
    }

    for (auto& prop : obj->properties) {
        do_indent(indent + 1);
        cout << '\'';
        cout_u8(get_sym_string(prop.first));
        cout << "' ";
        dump_object(prop.second, indent + 1);
    }

    for (auto dg : obj->delegated) {
        do_indent(indent + 1);
        cout << "(delegates) '";
        cout_u8(get_sym_string(dg->get(objects.s__name).value()));
        cout << '\'' << endl;
    }

    do_indent(indent);
    cout << "]]" << endl;
}

#pragma once

#include <cstddef>
#include <functional>
#include <gc_cpp.h>
#include <gmpxx.h>
#include <list>
#include <optional>
#include <unordered_map>
#include <unordered_set>
#include <variant>
#include <vector>

// native data types
using T_buf8 = std::vector<char8_t>;
using T_integer = mpz_class;
using T_real = double;
// (method object, context, receiver, args)
using T_command = std::function
    <void(
         class Object*, class Object*,
         class Object*, std::list<class Object*>)>;
using T_query = std::function
    <class Object*(
         class Object*, class Object*,
         class Object*, std::list<class Object*>)>;

// Sethlans object
// contains properties and delegated objects
class Object : public gc {
public:
    // optimized hash, ensures bits are all scrambled nicely
    struct Hasher {
        std::size_t operator()(Object* const& x) const noexcept;
    };

    // properties set for that object
    std::unordered_map<Object*, Object*, Hasher> properties;

    using Native_data = std::optional<
        std::variant<
            T_buf8,
            T_integer,
            T_real,
            T_command,
            T_query>>;

    // native data property
    Native_data native_data;

    // delegated objects, where properties can be looked up
    std::list<Object*> delegated;

    // delegate objects, delegating this object
    std::unordered_set<Object*, Hasher> delegates;

    // property resolution order, C3 linearization of delegated
    std::list<Object*> res_order;

    // update the resolution order
    void update_res_order();

    Object();

    // shallow clone object (props and delegates)
    Object* clone() const;

    // get a property
    std::optional<Object*> get(Object* name) const noexcept;

    // set a property
    void set(Object* name, Object* value);

    // get native data property
    Native_data& get_nd() noexcept;

    // set native data property
    void set_nd(Native_data data) noexcept;

    // delegate an object
    void delegate(Object* obj);

    // undelegate an object
    void undelegate(Object* obj);

    // check if it delegates an object
    bool does_delegate(Object* obj) const noexcept;
};

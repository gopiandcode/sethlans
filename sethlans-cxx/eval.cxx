#include "eval.hxx"
#include "object.hxx"
#include "objects.hxx"

#include <cassert>
#include <list>
#include <optional>
#include <stdexcept>
#include <type_traits>
#include <variant>

using namespace std;

enum class Method_type {
    None,
    Command,
    Query,
};

static Method_type get_method_type(Object* method, long* argc_r) {
    auto argc_ = method->get(objects.s__argcount);

    if (!argc_ || !argc_.value()->get_nd() ||
        !holds_alternative<T_integer>(argc_.value()->get_nd().value()))
        throw runtime_error("not a method");

    auto argc = get<T_integer>(argc_.value()->get_nd().value());

    if (argc < 0 || !argc.fits_slong_p())
        throw runtime_error("invalid argument count");

    if (argc_r)
        *argc_r = argc.get_si();

    auto q_ = method->get_nd();
    if (!q_)
        throw runtime_error("not a method");

    return visit([&](auto&& q) -> Method_type {
        using T = decay_t<decltype(q)>;
        if constexpr (is_same_v<T, T_command>) {
            return Method_type::Command;
        } else if constexpr (is_same_v<T, T_query>) {
            return Method_type::Query;
        } else {
            throw runtime_error("not a method");
        }
    }, q_.value());
}

static Method_type get_method(
    Object* self, Object* name, Object* context,
    long& argc_r, Object*& method_r,
    T_command* command_r, T_query* query_r,
    bool retry = true) {
    auto k_ = self->get(name);
    if (!k_) {
        if (retry) {
            list<Object*> nf_args;
            nf_args.push_back(name);
            long nf_argc;
            Object* nf_method;
            T_query nf_query;
            Object* nf_name;

            if (command_r && !query_r) {
                nf_name = objects.s_not_found_c;
            } else if (!command_r && query_r) {
                nf_name = objects.s_not_found_q;
            } else {
                assert(0);
            }

            auto t = get_method(
                self, nf_name, context,
                nf_argc, nf_method,
                nullptr, &nf_query, false);

            if (t != Method_type::Query)
                return Method_type::None;

            if (nf_argc != 1)
                throw runtime_error(
                    "not found handler must take 1 argument");

            method_r = nf_query(nf_method, context, self, nf_args);

            if (method_r == objects.g_nil)
                return Method_type::None;

            t = get_method_type(method_r, &argc_r);

            if (t == Method_type::Command)
                *command_r = get<T_command>(method_r->get_nd().value());
            else if (t == Method_type::Query)
                *query_r = get<T_query>(method_r->get_nd().value());

            return t;
        } else {
            return Method_type::None;
        }
    }
    auto method = method_r = k_.value();

    const auto t = get_method_type(method, &argc_r);

    switch (t) {
    case Method_type::Command:
        if (command_r)
            *command_r = get<T_command>(method->get_nd().value());
        break;

    case Method_type::Query:
        if (query_r)
            *query_r = get<T_query>(method->get_nd().value());
        break;

    default:
        break;
    }

    return t;
}

Object* Eval::terminal() {
    if (*code == objects.sx_backtick) {
        code++;
        if (code == List_iterator())
            throw runtime_error("unexpected eof");
        return *code++;
    } else if (*code == objects.sx_lparen) {
        code++;
        auto x = expr(t_self);
        if (code == List_iterator())
            throw runtime_error("unexpected eof");
        if (*code++ != objects.sx_rparen)
            throw runtime_error("expected closing parenthesis");
        return x;
    } else if (*code == objects.sx_backslash) {
        code++;
        if (code == List_iterator())
            throw runtime_error("unexpected eof");

        if ((*code)->does_delegate(objects.p_list)) {
            List_builder l;
            Eval ev(t_self, *code++);

            while (ev.code != List_iterator())
                l.append(ev.terminal());
            return l.finish();
        } else {
            return terminal();
        }
    } else if ((*code)->does_delegate(objects.p_symbol)) {
        Object* method;
        long argc;
        T_query query;
        const auto t = get_method(
            t_self, *code++, t_self, argc,
            method, nullptr, &query);

        if (t != Method_type::Query)
            throw runtime_error("not a query");

        if (argc != 0)
            throw runtime_error("terminal queries must not take arguments");

        return query(method, t_self, t_self, list<Object*>());
    } else {
        auto obj = *code++;
        Object* method;
        long argc;
        T_query query;
        get_method(
            obj, objects.s_clone_q, t_self, argc,
            method, nullptr, &query);
        return query(method, t_self, obj, list<Object*>());
    }
}

Object* Eval::expr(Object* self, bool first) {
    if (code == List_iterator()) {
        return self;
    } else if (*code == objects.sx_backtick ||
               *code == objects.sx_lparen ||
               *code == objects.sx_backslash) {
        if (!first)
            throw runtime_error("invalid terminal");
        return expr(terminal(), false);
    } else if (*code == objects.sx_rparen) {
        return self;
    } else if ((*code)->does_delegate(objects.p_symbol)) {
        Object* method;
        long argc;
        T_query query;
        const auto t = get_method(
            self, *code, t_self, argc,
            method, nullptr, &query);

        if (t != Method_type::Query)
            return self;

        list<Object*> args;
        code++;

        while (argc--)
            args.push_back(terminal());

        return expr(query(method, t_self, self, args), false);
    } else {
        if (!first)
            throw runtime_error("invalid terminal");
        return expr(terminal(), false);
    }
}

// TODO this code isn't as tail recursive as i'd like it to be
// or maybe gcc still figures out it's ok
// i doubt it
void Eval::command(Object*& self_r) {
    auto self = expr(t_self);

    self_r = nullptr;
    if (code == List_iterator()) {
        self_r = self;
        return;
    }

    if (!(*code)->does_delegate(objects.p_symbol))
        throw runtime_error("not symbol");

    Object* method;
    long argc;
    T_command command;
    const auto t = get_method(
        self, *code++, t_self, argc,
        method, &command, nullptr);

    if (t != Method_type::Command)
        throw runtime_error("not a command");

    list<Object*> args;

    while (argc--)
        args.push_back(terminal());

    return command(method, t_self, self, args);
}

void Eval::commands() {
    Object* self;
    while (code != List_iterator()) {
        command(self);
        if (self)
            throw runtime_error("unexpected eof");
    }
}

Object* Eval::query() {
    Object* self;
    while (code != List_iterator())
        command(self);
    return self ? self : t_self;
}

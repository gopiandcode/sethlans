#include "bootenv.hxx"
#include "eval.hxx"
#include "object.hxx"
#include "objects.hxx"

#include <cassert>
#include <gmpxx.h>
#include <iostream>
#include <list>
#include <stdexcept>
#include <string>
#include <variant>
#include <vector>

using namespace std;

template<typename T>
static Object* cmp(T x, T y) {
    if (x < y)
        return objects.s_lt;
    else if (x == y)
        return objects.s_eq;
    else if (x > y)
        return objects.s_gt;
    assert(0);
}

Object* make_bootenv() {
    auto env = new Object();
    env->delegate(objects.p_object);

    auto add_proto = [&](Object* p) {
        auto n = get<vector<char8_t>>(
            p->get(objects.s__name).value()->
            get(objects.s__name).value()->get_nd().value());
        auto m = u8string(n.begin(), n.end());
        make_const(env, m, p);
    };

    make_sg(objects.p_object, u8"name");
    make_sg(objects.p_list, u8"car");
    make_sg(objects.p_list, u8"cdr");
    make_sg(objects.p_method, u8"argcount");
    make_sg(objects.p_eval_method, u8"argnames");
    make_sg(objects.p_eval_method, u8"code");
    make_sg(objects.p_eval_method, u8"scope");
    make_sg(objects.p_accessor, u8"property");
    make_sg(objects.p_constant, u8"value");
    make_sg(objects.p_scope, u8"self");
    make_sg(objects.p_scope, u8"scope");
    make_sg(objects.p_scope, u8"context");

    make_sg(env, u8"nil", objects.g_nil);

    add_proto(objects.p_object);
    add_proto(objects.p_unique);
    add_proto(objects.p_symbol);
    add_proto(objects.p_buf8);
    add_proto(objects.p_string);
    add_proto(objects.p_list);
    add_proto(objects.p_integer);
    add_proto(objects.p_real);
    add_proto(objects.p_method);
    add_proto(objects.p_native_method);
    add_proto(objects.p_eval_method);
    add_proto(objects.p_command);
    add_proto(objects.p_query);
    add_proto(objects.p_accessor);
    add_proto(objects.p_native_command);
    add_proto(objects.p_native_query);
    add_proto(objects.p_setter);
    add_proto(objects.p_getter);
    add_proto(objects.p_eval_command);
    add_proto(objects.p_eval_query);
    add_proto(objects.p_constant);
    add_proto(objects.p_scope);

    auto add_command = [&](
        auto p, auto name, auto argc, T_command f) {
        auto obj = new Object();
        obj->delegate(objects.p_command);
        obj->delegate(objects.p_native_method);
        obj->set(objects.s__argcount, make_integer(argc));
        obj->set_nd(f);

        p->set(get_symbol(name), obj);
    };

    auto add_query = [&](
        auto p, auto name, auto argc, T_query f) {
        auto obj = new Object();
        obj->delegate(objects.p_query);
        obj->delegate(objects.p_native_method);
        obj->set(objects.s__argcount, make_integer(argc));
        obj->set_nd(f);

        p->set(get_symbol(name), obj);
    };

    add_command(
        env, u8"print!", 1,
        [](auto, auto, auto, auto args) {
            dump_object(args.front());
        });

    add_command(
        env, u8"eval-with!", 2,
        [](auto, auto, auto, auto args) {
            auto self = args.front();
            args.pop_front();
            auto code = args.front();
            return Eval(self, code).commands();
        });

    add_query(
        env, u8"eval-with?", 2,
        [](auto, auto, auto, auto args) {
            auto self = args.front();
            args.pop_front();
            auto code = args.front();
            return Eval(self, code).query();
        });

    add_command(
        env, u8"while!", 2,
        [](auto, auto context, auto, auto args) {
            auto cond = args.front();
            args.pop_front();
            auto body = args.front();
            while (Eval(context, cond).query() == objects.s_true)
                Eval(context, body).commands();
        });

    add_query(
        env, u8"command-with?", 3,
        [](auto, auto, auto, auto args) {
            auto scope = args.front();
            args.pop_front();
            auto argnames = args.front();
            args.pop_front();
            auto code = args.front();

            auto obj = new Object();
            obj->delegate(objects.p_eval_command);
            obj->set(
                objects.s__argcount,
                make_integer(list_length(argnames)));
            obj->set(objects.s__argnames, argnames);
            obj->set(objects.s__code, code);
            obj->set(objects.s__scope, scope);

            return obj;
        });

    add_query(
        env, u8"query-with?", 3,
        [](auto, auto, auto, auto args) {
            auto scope = args.front();
            args.pop_front();
            auto argnames = args.front();
            args.pop_front();
            auto code = args.front();

            auto obj = new Object();
            obj->delegate(objects.p_eval_query);
            obj->set(
                objects.s__argcount,
                make_integer(list_length(argnames)));
            obj->set(objects.s__argnames, argnames);
            obj->set(objects.s__code, code);
            obj->set(objects.s__scope, scope);

            return obj;
        });

    add_command(
        objects.p_command, u8"apply!", 3,
        [](auto, auto, auto method, auto i_args) {
            auto self = i_args.front();
            i_args.pop_front();
            auto context = i_args.front();
            i_args.pop_front();
            auto args = i_args.front();

            list<Object*> n_args;
            List_iterator it(args);
            while (it != List_iterator())
                n_args.push_back(*it++);

            return get<T_command>(method->get_nd().value())(
                method, context, self, n_args);
        });

    add_query(
        objects.p_query, u8"apply?", 3,
        [](auto, auto, auto method, auto i_args) {
            auto self = i_args.front();
            i_args.pop_front();
            auto context = i_args.front();
            i_args.pop_front();
            auto args = i_args.front();

            list<Object*> n_args;
            List_iterator it(args);
            while (it != List_iterator())
                n_args.push_back(*it++);

            return get<T_query>(method->get_nd().value())(
                method, context, self, n_args);
        });

    add_query(
        objects.p_object, u8"clone?", 0,
        [](auto, auto, auto self, auto) {
            return self->clone();
        });

    add_query(
        objects.p_list, u8"clone?", 0,
        [](auto, auto, auto self, auto) {
            List_iterator a(self);
            List_builder b;
            while (a != List_iterator())
                b.append(*a++);
            return b.finish();
        });

    add_query(
        objects.p_unique, u8"clone?", 0,
        [](auto, auto, auto self, auto) {
            return self;
        });

    add_command(
        objects.p_object, u8"set!", 2,
        [](auto, auto, auto self, auto args) {
            auto name = args.front();
            args.pop_front();
            auto value = args.front();
            self->set(name, value);
        });

    add_query(
        objects.p_object, u8"get?", 1,
        [](auto, auto, auto self, auto args) {
            auto name = args.front();
            return self->get(name).value();
        });

    add_query(
        objects.p_object, u8"has?", 1,
        [](auto, auto, auto self, auto args) {
            auto name = args.front();
            return self->get(name) ? objects.s_true : objects.s_false;
        });

    add_query(
        objects.p_object, u8"=", 1,
        [](auto, auto, auto x, auto args) {
            auto y = args.front();
            return x == y ? objects.s_true : objects.s_false;
        });

    add_command(
        objects.p_object, u8"delegate!", 1,
        [](auto, auto, auto self, auto args) {
            self->delegate(args.front());
        });

    add_query(
        objects.p_object, u8"mro", 0,
        [](auto, auto, auto self, auto) {
            List_builder lb;
            for (auto x : self->res_order)
                lb.append(x);
            return lb.finish();
        });

    add_query(
        objects.p_object, u8"not-found!", 1,
        [](auto, auto, auto self, auto args) {
            auto name_obj = args.front();
            auto name = get_sym_string(name_obj);

            if (name.at(name.size() - 1) == ':') {
                make_sg(self, u8string(name.begin(), name.end() - 1));
                return self->get(name_obj).value();
            } else {
                return objects.g_nil;
            }
        });

    add_query(
        objects.p_symbol, u8"make?", 1,
        [](auto, auto, auto, auto args) {
            auto x = get<T_buf8>(args.front()->get_nd().value());
            return get_symbol(u8string(x.begin(), x.end()));
        });

#define T_BINOP(type, op, name, mk)                             \
    add_query(                                                  \
        objects.p_ ## type, u8 ## name, 1,                      \
        [](auto, auto, auto x, auto args) {                     \
            auto y = args.front();                              \
            return mk(type)(                                    \
                op(                                             \
                    get<T_ ## type>(x->get_nd().value()),       \
                    get<T_ ## type>(y->get_nd().value())));     \
        })
#define INFIX(op) [](auto x, auto y) { return x op y; }
#define INFIX_SI(op) [](auto x, auto y) { return x op y.get_si(); }
#define MAKE(type) make_ ## type
#define PASS(type)

    T_BINOP(integer, INFIX(+), "+", MAKE);
    T_BINOP(integer, INFIX(-), "-", MAKE);
    T_BINOP(integer, INFIX(*), "*", MAKE);
    T_BINOP(integer, INFIX(/), "div", MAKE);
    T_BINOP(integer, INFIX(%), "rem", MAKE);
    T_BINOP(integer, INFIX(&), "andb", MAKE);
    T_BINOP(integer, INFIX(|), "orb", MAKE);
    T_BINOP(integer, INFIX(^), "xorb", MAKE);
    T_BINOP(integer, INFIX_SI(<<), "shlb", MAKE);
    T_BINOP(integer, INFIX_SI(>>), "shrb", MAKE);
    T_BINOP(integer, cmp<T_integer>, "<=>", PASS);

    T_BINOP(real, INFIX(+), "+", MAKE);
    T_BINOP(real, INFIX(-), "-", MAKE);
    T_BINOP(real, INFIX(*), "*", MAKE);
    T_BINOP(real, INFIX(/), "/", MAKE);
    T_BINOP(real, cmp<T_real>, "<=>", PASS);

    add_query(
        objects.p_buf8, u8"+", 1,
        [](auto, auto, auto x, auto args) {
            auto y = args.front();
            auto xn = get<T_buf8>(x->get_nd().value());
            auto& yn = get<T_buf8>(y->get_nd().value());
            xn.insert(xn.end(), yn.begin(), yn.end());

            auto obj = new Object();
            obj->delegate(objects.p_buf8);
            obj->set_nd(xn);
            return obj;
        });

    return env;
}

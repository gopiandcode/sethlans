# Sethlans

## Syntax

Due to Sethlans's homoiconicity (code is data), code is represented in
a generic data format (like, for example, LISP). This format is itself
parsed in two phases: tokenization and parsing.

### Tokenization

During the tokenization phase, a string is transformed into a list of
textual tokens. This is done through simple rules:

- Tokens are separated by any amount of whitespace. There may also be
  leading and trailing whitespace in the input string.
- Characters between `{` and `}` are comments, and are effectively
  removed from the input string. (So a comment inbetween two tokens do
  not necessarily separate them, if there's no whitespace around it.)
  Comments may also be nested.
- Characters may be escaped with `$`, followed by any character, or
  with `@`, followed by one or more hexadecimal digits and a `@` (or
  `#`), indicating a codepoint.
- Characters between two single quotes `'` are interpreted the same as
  outside of single quotes, except that whitespace is taken literally,
  as well as comment characters. Escapes still work, and like
  comments, quotes do not separate tokens by themselves.

"Whitespace" here indicates any codepoint less than or equal to
0x20. Unicode codepoints indicated with `@` are encoded as UTF-8 if
finished with `#`, else passed literally (for Unicode, codepoints must
be less than 0x110000, and less than 0x100 for bytes).

For example: `Hello,$ World`, `He{aven}llo','' ''W'orld` and
`@48@ello$,$ World` all represent the same token `Hello, World`.

### Parsing

During the parsing phase, a list of tokens is transformed into pokens
(parsed tokens). There are the following types of pokens:

- Objects, between `[[` and `]]`. Property of the object are
  enumerated as a literal token (the property name), followed by a
  poken (the property value). Duplicate properties are not allowed,
  and the object delegates `<object>` by default.
- Lists, between `[` and `]`. Any amount of pokens inbetween, which
  are the elements of the list.
- Strings, any token beginning with `"`. The initial double-quote
  character is not kept and the rest of the token is taken as a
  string.
- Integers, of the form `(sign)(base#)(number)`, where sign is the
  sign (`+` or `-`, optional), base is the base (between 2 and 36,
  defaults to 10, optional), and the number is made of lower or
  uppercase digits from the base's alphabet.
- Reals, of the form
  `(sign)(base#)(integer).(fractional)(^(sign)(exponent))`. The sign
  and base component are same as for integers, the integer and
  fractional part are in the specified base, and may both be omitted
  (`.` is a valid real, for 0.0). The exponent is always in base 10.
- Symbols, which are anything not parsed as another poken. A token may
  be forced to be parsed as a symbol by prefixing it with a backtick
  `, which is not included in the poken.

There may not be more than one top-level poken in a single list of
token.

### Evaluation

The actual evaluation is done on a list poken.

Code is divided into statements, each statement is made of a receiver,
command, and arguments.

The receiver is an expression, the arguments are terminals, and the
command is a symbol that specifies a command inside the receiver.

Expressions are made of a receiver (a terminal), followed by zero or
more queries onto this receiver, made of a query name (symbol), and
its arguments (all terminals).

Terminals may be any poken other than a symbol, which is interpreted
literally, a literal symbol by preceeding a symbol with a backtick
symbol (so double backtick for parsing), an expression between
parentheses, or the current object as the absence of anything.

The number of arguments for commands and queries is determined
dynamically during evaluation.

## Prototype system

Sethlens is a prototype-based language, like Self or JavaScript. This
means that everything is an object, but unlike classical
object-oriented languages such as Smalltalk or Python, there is no
distinction between classes and instances of classes.

Objects contain a number of properties, as well as delegated
objects. Properties are identified with a symbol, and have any object
as value.

Delegation works such that, in case a property is not found within the
object itself, the property is searched inside the delegated
objects. This allows a form of inheritance. However, properties are
always set within the object itself, not in any delegated object.

## Commands and queries

Sethlans enforces command-query separation. This means that a method
can either be a command, or a query.

A command does something, and does not have a return value. A query
returns something, and should not have side effects (this is not
enforced by the language, however).

By convention, commands have a name ending with `!`, and queries a
name ending with `?`. However, property getters have no suffix, and
property setters end with `:`.

## Scoping

Sethlans does not have any specific concept of variables, or lexical
or dynamic scoping. Instead, everything acts, either explicitely or
implicitely, on an object. The implicit object is called the context.

For example, the global scope is an object, containing all the basic
functions and prototype objects.

Inside functions and standard control structures, there is a dedicated
context, delegating the object the function it is called on
(explicitely accessible via `self`) and the context it was defined in
(that is, the object the function was defined on; accessible via
`scope`), in that order. This allows closures, as well as extensible
scoping.

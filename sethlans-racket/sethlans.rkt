#lang racket/base
;; * Configuration
;; ** Setup
(require racket/match
         racket/function
         racket/string)
(require parser-tools/lex
         (prefix-in re- parser-tools/lex-sre)
         parser-tools/yacc)
(provide read-nested-comment)
;; ** Testing configuration
;; Setup testing environment
(module+ test
  (require rackunit)
  (define (iterate fn final-value)
    (define (loop acc)
      (match (fn)
        [v #:when (eq? v final-value) (reverse acc)]
        [v (loop (cons v acc))]))
    (loop '()))
  (define (iterate-pred fn prd)
    (define (loop acc)
      (match (fn)
        [v #:when (prd v) (reverse acc)]
        [v (loop (cons v acc))]))
    (loop '()))
  (define-syntax run-with-input-string
    (syntax-rules ()
      [(run-with-input-string (fn ...) str)
       (fn ... (open-input-string str))]))
  (define default-pos (position 0 0 0))
  (define-syntax to-list
    (syntax-rules ()
      [(to-list expr) (call-with-values (thunk expr) list)]))
  (define (lexer-to-list lexer str)
    (define port (open-input-string str))
    (define (fn) (to-list (lexer port)))
    (iterate-pred fn (lambda (ls) (eq? (token-name (position-token-token (car ls))) 'EOF)))))
;; * Lexer
;; ** Definitions
(define-lex-abbrevs
  [whitespace (char-set " \t\n")]
  [digit (char-set "0123456789")]
  [non-zero-digit (char-set "123456789")]
  [sign (char-set "+-")]
  [number
   (union (re-: non-zero-digit (re-* digit))
          "0")]
  [base (re-: number "#")]
  [integer (re-: (re-? sign) (re-? base) number)]
  [real (re-: (re-? sign) (re-? base) (re-? number)
              "." (re-? number) (re-? (re-: "^" (re-? sign) number)))]
  [literal-char (union (char-complement (char-set "${}'")) escaped-char)]
  [escaped-char (re-: "$" (char-complement (union)))]
  [non-escaped-char (char-complement (char-set "{} \t\n'[]()$"))]
  [lexeme-char (union non-escaped-char escaped-char)]
  [non-lexeme-char (union whitespace "[[" "]]" "[" "]" "(" ")" "`")])
;; ** Tokens
(define-tokens sethlans-tokens
  (INTEGER REAL SYMBOL))
(define-empty-tokens sethlans-delimiter-tokens
  (LIST-OPEN LIST-CLOSE OBJECT-OPEN OBJECT-CLOSE LPAREN RPAREN EOF))
;; ** Helper functions
(define (strip-last-char str)
  (substring str 0 (- (string-length str) 1)))

(define (strip-first-char str)
  (substring str 1 (string-length str)))

(define (strip-first-and-last-char str)
  (substring str 1 (- (string-length str) 1)))

(define (ret lexeme type start-pos end-pos)
  (values lexeme type (position-offset start-pos) (position-offset end-pos)))
;; ** Lexing functions
;; *** Comment
;;; Retrieves the next comment-character
(define get-next-comment
  (lexer
   ["{" (values 1 end-pos)]
   ["}" (values -1 end-pos)]
   [(re-or
     escaped-char
     (char-complement (char-set "{}$")))             ;; non-escaped char
    (get-next-comment input-port)]
   [(eof) (values 'eof end-pos)]))

;; Reads comments, handles nesting
(define (read-nested-comment num-opens start-pos input)
  (let-values (((diff end) (get-next-comment input)))
    (cond
      [(eq? 'eof diff) (ret "Unterminated comment" 'error start-pos end)]
      [else
       (let ((next-num-opens (+ diff num-opens)))
         (cond
           [(= 0 next-num-opens) (ret "" 'comment start-pos end)]
           [else (read-nested-comment next-num-opens start-pos input)]))])))

(module+ test
  (check-equal?
   (to-list (run-with-input-string
             (read-nested-comment 0 default-pos) "{a comment} at the start."))
   (list "" 'comment 0 12)
   "Basic comments read correctly.")
  (check-equal?
   (to-list (run-with-input-string
             (read-nested-comment 0 default-pos) "{a 'comment' with a literal} at the start."))
   (list "" 'comment 0 29 )
   "Comments with literals read correctly.")
  (check-equal?
   (to-list (run-with-input-string
             (read-nested-comment 0 default-pos) "{a comment with a 'literal} at the 'start'."))
   (list "" 'comment 0 28)
   "Who the hell would do this? and why is it allowed by the spec?.")
  (check-equal?
   (to-list (run-with-input-string
             (read-nested-comment 0 default-pos) "{a ${ $}comment} at the start."))
   (list "" 'comment 0 17)
   "Escaped comments read correctly.")
  (check-equal?
   (to-list (run-with-input-string
             (read-nested-comment 0 default-pos) "{a comment at the that is not completed."))
   (list "Unterminated comment" 'error 0 41)
   "Invalid comment comments read correctly."))

;; *** Literals
(define (read-literal acc)
  (lexer
   [(re-:
     ;; sequence of one or more literal chars
     (re-* literal-char)
     ;; followed by
     "'")
    (values end-pos (string-join (reverse (cons lexeme acc)) ""))]
   ;; handle comments inside literals
   [(re-:
     ;; sequence of one or more literal chars
     (re-* literal-char)
     ;; followed by comment start
     "{")
    (begin
      (read-nested-comment 1 start-pos input-port)
      ((read-literal (cons (strip-last-char lexeme) acc)) input-port))]))

(module+ test
  (check-equal?
   (to-list (run-with-input-string
             ((read-literal '())) " a basic literal '"))
   (list (position 19 #f #f) " a basic literal '")
   "Basic literals read correctly.")
  (check-equal?
   (to-list (run-with-input-string
             ((read-literal '())) " a literal with escapes $' but still read'"))
   (list (position 43 #f #f) " a literal with escapes $' but still read'")
   "Literals with escapes correctly.")
  (check-equal?
   (to-list (run-with-input-string
             ((read-literal '())) " a literal with comments { this is a comment }but fine'"))
   (list (position 56 #f #f) " a literal with comments but fine'")
   "Literals with comments correctly.")
  (check-equal?
   (to-list (run-with-input-string
             ((read-literal '())) " a literal with nested comments { this is a {comment} }but fine'"))
   (list (position 65 #f #f) " a literal with nested comments but fine'")
   "Literals with comments correctly."))

;; *** Tokens
;;; Reads tokens
(define (read-token start-pos acc)
  (lexer-src-pos
   ;; comments
   [(re-: (re-* lexeme-char) "{")
    (begin
      (read-nested-comment 1 end-pos input-port)
      (return-without-pos
       ((read-token start-pos (cons (strip-last-char lexeme) acc)) input-port)))]
   ;; literals
   [(re-: (re-* lexeme-char) "'")
    (begin
      (let-values (((_ literal) ((read-literal '()) input-port)))
        (return-without-pos
         ((read-token start-pos (cons literal (cons lexeme acc))) input-port))))]
   ;; tokens
   [(re-: "`" (re-+ lexeme-char))
    (let ((token (string-join (reverse (cons lexeme acc)) "")))
      (return-without-pos
       (position-token (token-SYMBOL token) start-pos end-pos)))]
   [(re-+ lexeme-char)
    (begin
      (let ((token (string-join (reverse (cons lexeme acc)) "")))
      (return-without-pos
       (position-token (token-SYMBOL token) start-pos end-pos))))]
   ;; whitespace
   [(re-:)
    (let ((token (string-join (reverse (cons lexeme acc)) "")))
      (return-without-pos
       (position-token (token-SYMBOL token) start-pos end-pos)))]
   [(eof)
    (let ((token (string-join (reverse acc) "")))
      (return-without-pos
       (position-token (token-SYMBOL token) start-pos end-pos)))]))

(module+ test
  (check-equal?
   (to-list (run-with-input-string
             ((read-token default-pos '())) "a-basic-token"))
   (list
    (position-token
     (token-SYMBOL "a-basic-token")
     (position 0 0 0)
     (position 14 #f #f)))
"Basic tokens read correctly.")
  (check-equal?
   (to-list (run-with-input-string
             ((read-token default-pos '())) "why$ on$ earth$ would$ spaces$ occur$ in an$ identifier"))
   (list
    (position-token
     (token-SYMBOL "why$ on$ earth$ would$ spaces$ occur$ in")
     (position 0 0 0)
     (position 41 #f #f)))
   "Tokens with escaped characters read correctly.")
  (check-equal?
   (to-list (run-with-input-string
             ((read-token default-pos '())) "what-sort-of-{trash-}language-spec-allows-spaces-in-lexemes"))
   (list
    (position-token
     (token-SYMBOL "what-sort-of-language-spec-allows-spaces-in-lexemes")
     (position 0 0 0)
     (position 60 #f #f)))
   "Tokens with literals read correctly.")
  (check-equal?
   (to-list (run-with-input-string
             ((read-token default-pos '())) "what' sort of trash language spec allows spaces in lexemes '"))
   (list
    (position-token
     (token-SYMBOL
      "what' sort of trash language spec allows spaces in lexemes '")
     (position 0 0 0)
     (position 61 #f #f)))
   "Tokens with literals read correctly."))

;; *** Main Lexer

(define lex-sethlans
  (lexer-src-pos
   ["{"
    (begin
      (read-nested-comment 1 end-pos input-port)
      (return-without-pos (lex-sethlans input-port)))]
   ["(" (token-LPAREN)]
   [")" (token-LPAREN)]
   ;; numbers
   ;; NOTE: Q: Can there be comments in numbers?
   ;;       A: NO. THAT IS A TERRIBLE IDEA.
   [integer (token-INTEGER lexeme)]
   [real (token-REAL lexeme)]
   ["[[" (token-OBJECT-OPEN)]
   ["]]" (token-OBJECT-CLOSE)]
   ["[" (token-LIST-OPEN)]
   ["]" (token-LIST-CLOSE)]
   ;; tokens
   [(re-: "`" (re-+ lexeme-char))
    (return-without-pos
     ((read-token start-pos (list (strip-first-char lexeme))) input-port))]
   ;; literals
   [(re-+ lexeme-char)
    (return-without-pos ((read-token start-pos (list lexeme)) input-port))]
   ;; comments
   [whitespace (return-without-pos (lex-sethlans input-port))]
   [(eof) (token-EOF)]))

(module+ test
  (check-equal?
   (lexer-to-list lex-sethlans "a series of tokens.")
   (list
    (list
     (position-token (token-SYMBOL "a") (position 1 #f #f) (position 2 #f #f)))
    (list
     (position-token
      (token-SYMBOL "series")
      (position 3 #f #f)
      (position 9 #f #f)))
    (list
     (position-token
      (token-SYMBOL "of")
      (position 10 #f #f)
      (position 12 #f #f)))
    (list
     (position-token
      (token-SYMBOL "tokens.")
      (position 13 #f #f)
      (position 20 #f #f)))))
  (check-equal?
   (lexer-to-list lex-sethlans "a series 10.0 of 2#10 tokens with numbers .")
   (list
    (list
     (position-token (token-SYMBOL "a") (position 1 #f #f) (position 2 #f #f)))
    (list
     (position-token
      (token-SYMBOL "series")
      (position 3 #f #f)
      (position 9 #f #f)))
    (list
     (position-token
      (token-REAL "10.0")
      (position 10 #f #f)
      (position 14 #f #f)))
    (list
     (position-token
      (token-SYMBOL "of")
      (position 15 #f #f)
      (position 17 #f #f)))
    (list
     (position-token
      (token-INTEGER "2#10")
      (position 18 #f #f)
      (position 22 #f #f)))
    (list
     (position-token
      (token-SYMBOL "tokens")
      (position 23 #f #f)
      (position 29 #f #f)))
    (list
     (position-token
      (token-SYMBOL "with")
      (position 30 #f #f)
      (position 34 #f #f)))
    (list
     (position-token
      (token-SYMBOL "numbers")
      (position 35 #f #f)
      (position 42 #f #f)))
    (list
     (position-token (token-REAL ".") (position 43 #f #f) (position 44 #f #f)))))
  (check-equal?
   (lexer-to-list lex-sethlans "a series [of] tokens.")
   (list
    (list
     (position-token (token-SYMBOL "a") (position 1 #f #f) (position 2 #f #f)))
    (list
     (position-token
      (token-SYMBOL "series")
      (position 3 #f #f)
      (position 9 #f #f)))
    (list (position-token 'LIST-OPEN (position 10 #f #f) (position 11 #f #f)))
    (list
     (position-token
      (token-SYMBOL "of")
      (position 11 #f #f)
      (position 13 #f #f)))
    (list (position-token 'LIST-CLOSE (position 13 #f #f) (position 14 #f #f)))
    (list
     (position-token
      (token-SYMBOL "tokens.")
      (position 15 #f #f)
      (position 22 #f #f)))))
  (check-equal?
   (lexer-to-list lex-sethlans "a series [[of]] tokens.")
   (list
    (list
     (position-token (token-SYMBOL "a") (position 1 #f #f) (position 2 #f #f)))
    (list
     (position-token
      (token-SYMBOL "series")
      (position 3 #f #f)
      (position 9 #f #f)))
    (list (position-token 'OBJECT-OPEN (position 10 #f #f) (position 12 #f #f)))
    (list
     (position-token
      (token-SYMBOL "of")
      (position 12 #f #f)
      (position 14 #f #f)))
    (list (position-token 'OBJECT-CLOSE (position 14 #f #f) (position 16 #f #f)))
    (list
     (position-token
      (token-SYMBOL "tokens.")
      (position 17 #f #f)
      (position 24 #f #f))))
   '(("a" token 1 2)
     ("series" token 3 9)
     ("[[" object-open 10 12)
     ("of" token 12 14)
     ("]]" object-close 14 16)
     ("tokens." token 17 24)))
  (check-equal?
   (lexer-to-list lex-sethlans "a sequence rejected by any non-in{yes, very {insane}}sane language definition.")
   (list
    (list
     (position-token (token-SYMBOL "a") (position 1 #f #f) (position 2 #f #f)))
    (list
     (position-token
      (token-SYMBOL "sequence")
      (position 3 #f #f)
      (position 11 #f #f)))
    (list
     (position-token
      (token-SYMBOL "rejected")
      (position 12 #f #f)
      (position 20 #f #f)))
    (list
     (position-token
      (token-SYMBOL "by")
      (position 21 #f #f)
      (position 23 #f #f)))
    (list
     (position-token
      (token-SYMBOL "any")
      (position 24 #f #f)
      (position 27 #f #f)))
    (list
     (position-token
      (token-SYMBOL "non-insane")
      (position 28 #f #f)
      (position 58 #f #f)))
    (list
     (position-token
      (token-SYMBOL "language")
      (position 59 #f #f)
      (position 67 #f #f)))
    (list
     (position-token
      (token-SYMBOL "definition.")
      (position 68 #f #f)
      (position 79 #f #f))))))
;; * Parser
;; ** AST
;; It's called an AST, wtf is a POKEN?
(define-struct sethlans-list (elts) #:transparent)
(define-struct sethlans-object (properties) #:transparent)
(define-struct sethlans-real (n) #:transparent)
(define-struct sethlans-integer (n) #:transparent)
(define-struct sethlans-symbol (n) #:transparent)
(define-struct sethlans-statement (expr command args) #:transparent)

;; ** Grammar 
(define (to-src-loc pos)
  (make-srcloc #f
               (position-line pos)
               (position-col pos)
               (position-offset pos)
               #f))
;; A beautiful parser. Look at it's elegance.
(define sethlans-parser (parser
                         (src-pos)
                         (error
                          (lambda (tok-ok? tok-name tok-value start-pos end-pos)
                            (raise (make-exn:fail:read
                                    "Syntax error"
                                    (current-continuation-marks)
                                    (list
                                     (to-src-loc start-pos)
                                     (to-src-loc end-pos))))))
                         (tokens sethlans-tokens sethlans-delimiter-tokens)
                         (end EOF)
                         (start list)
                         (grammar
                          (list ((LIST-OPEN expr-seq LIST-CLOSE)
                                 (make-sethlans-list $2))
                                ((LIST-OPEN LIST-CLOSE)
                                 (make-sethlans-list null)))
                          (object ((OBJECT-OPEN property-seq OBJECT-CLOSE)
                                   (make-sethlans-object $2))
                                  ((OBJECT-OPEN OBJECT-CLOSE)
                                   (make-sethlans-object null)))
                          (expr-seq ((expr expr-seq) (cons $1 $2))
                                    ((expr) (list $1)))
                          (property-seq ((property property-seq) (cons $1 $2))
                                        ((property) (list $1)))
                          (terminal-seq ((terminal terminal-seq) (cons $1 $2))
                                        ((terminal) (list $1)))
                          (property ((SYMBOL expr) (cons (make-sethlans-symbol $1) $2)))
                          
                          (terminal
                           ((SYMBOL) (make-sethlans-symbol $1))
                           ((LPAREN expr RPAREN) $2))
                          (expr
                           ((list) $1)
                           ((object) $1)
                           ((SYMBOL) (make-sethlans-symbol $1))
                           ((INTEGER) (make-sethlans-integer $1))
                           ((REAL) (make-sethlans-real $1))))))
(module+ test
  (define-syntax with-lexer
    (syntax-rules ()
      [(with-lexer str (expr ...))
       (let ((input (open-input-string str)))
         (expr ... (lambda () (lex-sethlans input))))]
      [(with-lexer str expr)
       (let ((input (open-input-string str)))
         (expr (lambda () (lex-sethlans input))))]))
  (check-equal?
   (with-lexer "[ a b c d ]" sethlans-parser)
   (sethlans-list
    (list
     (sethlans-symbol "a")
     (sethlans-symbol "b")
     (sethlans-symbol "c")
     (sethlans-symbol "d")))
   "Parses lists correctly.")
  (check-exn
   exn:fail:read?
   (lambda () (with-lexer "[ a b c d" sethlans-parser))
   "Raises syntax error on incomplete lists.")
  (check-equal?
   (with-lexer "[ {a} [[b e]] c d ]" sethlans-parser)
   (sethlans-list
    (list
     (sethlans-object (list (cons (sethlans-symbol "b") (sethlans-symbol "e"))))
     (sethlans-symbol "c")
     (sethlans-symbol "d")))
   "Can read objects correctly.")
  (check-exn
   exn:fail:read?
   (lambda () (with-lexer "[ {a} [[b]] c d ]" sethlans-parser))
   "Raises exception on incomplete property.")
  (check-exn
   exn:fail:read?
   (lambda () (with-lexer "[ {a} [[b c c d ]" sethlans-parser))
   "Raises exception on unterminated object."))
;; * Evaluation

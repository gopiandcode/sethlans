# Sethlans

Sethlans (named after the Etruscan god of craftsmanship) is a dynamic,
prototype-based, functional programming language. It's made as a
testbed for some of my programming language ideas (including related
to Bjoern, my attempt at a high-level OS).

See doc.md for a description of the basic syntax and semantics.

## Interpreter

There are currently two interpreters: a basic one written in Python,
and another one in C++ being worked on (which will become the primary
interpreter).

The dependencies of the C++ interpreter are libgmp and libgc.

## Example

```
[

{ the classic introduction }
print! '"Hello, World!'

{ checks if a number is prime using trial division }
def-query! `` is-prime? [ n ] [
    m: 2
    prime: `` true
    while! [ return: ( m < n ) ] [
        if! ( n divisible-by? m ) [
            self2 prime: `` false
        ] [ ]
        self m: ( m + 1 )
    ]
    return: prime
]

{ enumerate all primes up to 100 }
i: 2
while! [ return: ( i < 100 ) ] [
    if! ( is-prime? i ) [
        print! i
    ] [ ]
    self i: ( i + 1 )
]

]
```
